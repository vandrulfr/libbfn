#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct no
{

  int info; // Elemento
  struct no *ant, *prox; // Ponterio anterior e proximo

}TNO;

typedef struct lcd
{
	char sinal; // Sinal em char
	TNO *ini, *fim; // Ponteiro em descritor para final e inicio da lista.
	int tamanho; //tamanho

}TLDE;

/* Funções basicas */

// Inicializar a lista
TLDE* inicializa(void);
// Insere elementos no fim da lista
void inserir_fim(TLDE *l, int x);
// imprime a lista do inicio -> fim
void imprime(TLDE *l);
// Função para receber o numero do usuario no formato de string
char *escrever_numero(void);
// insere os elementos de dois numero em formato de string em duas lista diferente, criado anteriomente
TLDE *criar_lista(char *string);
// libera as listas criadas
void libera_lista(TLDE* l);
// insere elementos do inicio da lista
void insere_inicio(TLDE *l, int x);
// imprimi a lista do fim -> inicio
void imprime_r(TLDE *l); // Criada somente para testar se o ponterio de anterior esta funcionando.
// compara sinais
int comp_sin(TLDE *l1, TLDE *l2);
// compara comprimento das listas
int comp_tam(TLDE *l1, TLDE *l2);

/* Soma */

TLDE *soma(TLDE *l1, TLDE *l2);

/* Divisão */

// Trasforma uma parte de uma lista em um numero inteiro
int trasformar_inteiro(TLDE *l, int x);
// Retira os x primerios elementos da lista
void retirar_elementos(TLDE * l, int x);
// Duplica a lista
TLDE *duplicar_lista(TLDE *l);
// Retorna o tamanho da lista
int tamanho_lista(TLDE *l);
// Divisão para numeros grandes por numeros pequenos
TLDE *divisao2(TLDE *l1, TLDE *l2);
// Multiplicar um unico numero por uma lista.
TLDE *multiplicar_um_elem(TLDE *l, int x);
// Compara dois numeros. Retorna quem é o maior ou se são iguais. Sem levar em consideração o sinal
int comparar_numero_ssinal(TLDE *l1, TLDE *l2);
// Insere somente um elemento que sera o 0. Usado somente para listas completamente vazias.
void primeiro_zero(TLDE *l);
// Função Final de divisão
TLDE *divisao(TLDE *l1, TLDE *l2);

/* Multiplicação */

// Insere 0 no final da lista. A lista não pode esta vazia
void zero_final(TLDE *l, int x);
// Função de Multiplicação
TLDE *multiplicar(TLDE *l1, TLDE *l2);


/* Subtração */
TLDE *sub(TLDE *l1, TLDE *l2);

