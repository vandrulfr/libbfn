#include "TRLDE.h"

TLDE* inicializa(void){

	/* Função para inicializar uma lista com os descritores. */

	TLDE *novo = (TLDE *) malloc (sizeof(TLDE));
	novo -> ini = NULL; // Inicializar o ponteiro do inicio
	novo -> fim = NULL; // Inicializar o ponteiro do final
	novo -> tamanho = 0; // tamanho
	return novo;

}

void primeiro_zero(TLDE *l){

	// Inserir o primero elementento na lista. Só insere um elemento (0) e a lista deve esta vazia.

	TNO *novo = (TNO*) malloc (sizeof(TNO));
	novo -> info = 0;
	novo -> ant = NULL;
	novo -> prox = NULL;
	l -> ini = novo;
	l -> fim = novo;
	
}


void inserir_fim(TLDE *l, int x){

	/* Função para inserir elementos na lista no final. */

	if(!l->ini){
		if(x!=0){
			// Utilizado apenas quando a lista esta vazia. Somente se o primeiro elemento não for zero.
			TNO *novo = (TNO*) malloc (sizeof(TNO));
			novo -> info = x;
			novo -> ant = NULL;
			novo -> prox = NULL;
			l -> ini = novo;
			l -> fim = novo;
			l ->  tamanho++;
			return;
		}
		else return;
	}

	TNO *novo = (TNO*)malloc(sizeof(TNO));
	novo -> info = x;
	novo -> prox = NULL;
	TNO *p = l -> ini;
	while(p -> prox != NULL) p = p -> prox;
	p -> prox = novo;
	novo -> ant = l -> fim;
	l -> fim = novo;
	l -> tamanho++;

}

void insere_inicio(TLDE *l, int x){

	/* Função para inserir elementos no inicio. */

	if(!l->ini){ // Se a lista esta vazia
		TNO *novo = (TNO*) malloc (sizeof(TNO));
		novo -> info = x;
		novo -> ant = NULL;
		novo -> prox = NULL;
		l -> ini = novo;
		l -> fim = novo;
		l -> tamanho++;
	}
	else{
		TNO *novo = (TNO*) malloc (sizeof(TNO)); // Alocar 
		novo -> info = x;
		novo -> ant = NULL;  // Ponteiro anteiro sempre tem que NULL do primeiro elemento da lista.
		novo -> prox = l -> ini;
		TNO *p = l -> ini;
		p -> ant = novo;
		l -> ini = novo;
		l -> tamanho++;
	}
}

void imprime(TLDE *l){

	/* Imprimir lista. */

	TNO *p = l -> ini;

	char positivo = '+';

	if(l->sinal != positivo) printf("%c", l -> sinal); // Imprime o sinal da lista

	while(p != NULL){
		printf("%d", p -> info);
		p = p -> prox;
	}
	printf("\n"); // Pula linha
}

char *escrever_numero(void){

	/* Escrever o numero em formato de string. Somente um número. */

	char *str = (char*) malloc (sizeof(char)*500); // Alocação dinamica

	printf("Escreva um numero: ");
	scanf("%s", str);

	printf("\n");

	return str;
}

TLDE *criar_lista(char *string){

	TLDE *resp = inicializa();
	int i;

	if(string[0] != '-'){

		resp -> sinal = '+';
		i = 0;
	}

	else{

		resp -> sinal = '-';
		i = 1;
	}

	while(string[i] != '\0'){


		char x = string[i];

		if((x == '1') || (x == '2') || (x == '3') || (x == '4') || (x == '5') || 
			(x == '6') || (x == '7') || (x == '8') || (x == '9') || (x == '0')){

			int n = x - '0';
			inserir_fim(resp, n);
			i = i + 1;
		}
		else i = i + 1;

		if(x == ','){

			if(!resp -> ini) primeiro_zero(resp);

			return resp;
		}
	}

	if(!resp -> ini) primeiro_zero(resp);

	return resp;
}

void libera_lista(TLDE* l){

	// Não funciona como deveria.

	// Liberar lista. 

	TNO *p = l -> ini, *q;

	while(p){
		q = p;
		p = p -> prox;
		free(q);
	}
	free(p);
	free(l);

}

TLDE *multiplicar_um_elem(TLDE *l, int x){

	/* Essa função serve para multiplicar uma lista por apenas uma numero. 
	EX.: 123 * 2 = 246
	onde 123 -> seria a lista de entrada, 2 -> é o x onde e resp -> a lista de resposta da multiplicação

	até o momento a funçao retorna uma lista sem o sinal positivo ou negativo.
	*/

	TLDE *resp = inicializa(); // Inicializando uma lista de resposta
	int n, add = 0, resto;
	TNO *p = l -> fim;
//	while(p -> prox != NULL) p = p -> prox; // essa linha a anterior serve para localizar o ultimo elemento da lista
	while(p){
		n = (p -> info * x) + add;
		add = 0;
		if (n>=10){ // numero maior ou igual a 10
			add = (n/10); // Numero que é soma na proxima casa. 
			resto = n - (add *10); // calculo do numero que fica a casa que estamos calculando agora. EX.: 12 -> 2 fica e vai 1
			insere_inicio(resp, resto);
		}
		if (n<10) insere_inicio(resp, n);
		p = p -> ant;		
	}
	if (add != 0) insere_inicio(resp, add); // acontece somente quando a ultima casa calculada tem a resposta >= 10.
	return resp;
}

void imprime_r(TLDE *l){

	// Imprime a lista de forma reversa. Utilizado apenas para testar se o ponteiro anterior esta funcionando.
	// Pode apagar ao final do trabalho

	TNO *p = l -> fim;
	while(p){
		printf("%d", p->info );
		p = p-> ant;
	}
}
 
int comparar_numero_ssinal(TLDE *l1, TLDE *l2){

	/*
	Funão para comparar duas listas ignorando o sinal
	0 = iguais
	1 = l1 > l2
	2 = l2 > l1
	*/

	int cont = 0, taml1, taml2, primeirol1, primeirol2;

	TNO *p = l1 -> ini;

	primeirol1 = p -> info;

	while(p -> prox != NULL){ // Calcular o tamanho da primeira lista
		p = p -> prox;
		cont += 1;
	}
	taml1 = cont;
	cont = 0;

	p = l2->ini;
	primeirol2 = p -> info;
	while(p -> prox != NULL){ // Calcular o tamanho da segunda lista 
		p = p -> prox;
		cont += 1;
	}

	taml2 = cont;

	if(taml2 == taml1){ // Se os dois tamanho forem iguais 

		if(primeirol1 > primeirol2) return 1; // comparar o primeiro elemento
		if(primeirol2 > primeirol1) return 2; // Comparar o primeiro elemento

		TNO *q = l1 -> ini;
		p = l2 -> ini;

		cont = 0;
		int aux = 0;

		while(p != NULL){
			if(p -> info > q -> info) return 2;
			if(q -> info > p -> info) return 1;
			if(p -> info == q -> info) cont += 1;
			p = p -> prox;
			q = q -> prox;
		}

		if(cont!=0) return 0;

	}

	if (taml2 > taml1) return 2;
	if (taml1 > taml2) return 1;

}

void zero_final(TLDE *l, int x){

	/* Insere 0 no final da lista x vezes. A lista não pode esta vazia. */

	int i;
	if(x!=0) for(i = 0; i<x; i++) inserir_fim(l, 0);
}

int comp_sin(TLDE *l1, TLDE *l2) 
{	//retorna 1 se sinais forem iguais
	if (l1 -> sinal != l2 -> sinal)
	{
		return(0);
	}
	else
	{
		return(1);
	}
}

int comp_tam(TLDE *l1, TLDE *l2){
/*
*	0 = iguais
*	1 = l1 > l2
*	2 = l2 > l1
*/
	if(l1->tamanho == l2->tamanho){return(0);}
	else if (l1->tamanho > l2->tamanho) {return (1);}
	else return (2);
}

TLDE *soma(TLDE *l1, TLDE *l2)
{
	TNO *index1 = l1->fim, *index2 = l2->fim;
	TLDE *resp_soma = inicializa();
	int aux=0, num_maior = comparar_numero_ssinal(l1,l2), i, sinal_comparado = comp_sin(l1,l2),tamanho_comparado = comp_tam(l1,l2), j, diftam;
	if (sinal_comparado) 	
	{	//se os sinais forem iguais, só soma
		while (tamanho_comparado)
		{ //compara tamanho das listas e insere 0 a esquerda da menor
			if (tamanho_comparado>1){insere_inicio(l1,0);}
			else{insere_inicio(l2,0);}
			tamanho_comparado = comp_tam(l1,l2);
		}
		
		resp_soma -> sinal = l1 -> sinal; //define o sinal do resultado como o sinal da primeira lista
		while (index1)
		{	//enquanto o index não chegar ao começo da lista:
			i = index1->info+index2->info+aux; //variavel recebe soma dos digitos + sobra da soma anterior
			aux = 0; //variavel que guarda sobra é resetada
			if (i > 9) //caso a soma seja maior que nove, subtrai 10 e sobe 1
			{
				i-=10;
				aux++;
			}
			insere_inicio(resp_soma, i); //caractere resultante vai pro começo da lista
			index1=index1->ant; //index1 e 2 se movem pra trás
			index2=index2->ant;
		}
		if (aux) //ao sair do loop, se ainda sobrar 1, vai pro começo do resultado
		{
			insere_inicio(resp_soma, aux);
			aux = 0;
		}
		return resp_soma;
	}
	else
	{
	
		if (!num_maior>1)
		{
			TLDE *L = duplicar_lista(l2); L->sinal = l1->sinal;
			return(sub(l1,L));
		}
		else
		{
			TLDE *L = duplicar_lista(l1); L->sinal = l2->sinal;
			return(sub(l2,L));
		}
	}
}

TLDE *multiplicar(TLDE *l1, TLDE *l2){

	TNO *lista = l2 -> fim; // listo dois sera percorrida
	TLDE *resp = inicializa(), *soma1 = inicializa();
	int contagem = 0; // Contagem de 0 no final de cada "linha" da multiplicação

	primeiro_zero(resp); // Lista de resposta começa com 0

	while(lista){ // Enquanto l2 existir

		soma1 = multiplicar_um_elem(l1, lista -> info); //Multiplica l1 por cada elemento de l2
		zero_final(soma1, contagem); // Coloca 0 no final
		contagem += 1; // Contagem de 0 no final da linha
		resp = soma(resp, soma1); // soma o que tinha com a nova linha da multiplicação
		lista = lista -> ant; // anda do fim para o inicio na lista 2.
	} 

	libera_lista(soma1);
	free(lista);

	// Sinal da lista resposta
	char positivo = '+', negativo = '-';
	resp -> sinal = '+';
	if((l1 -> sinal == negativo) || (l2 -> sinal == negativo)) resp -> sinal = '-'; // Se o sinal de alguma lista for negativo a respota sera negativa
	if((l1 -> sinal == negativo) && (l2 -> sinal == negativo)) resp -> sinal = '+'; // Se l1 e l2 é negativo o sinal de resposta é positivo.

	return resp;

}

TLDE *sub(TLDE *l1, TLDE *l2){

        /* 0 = iguais
           1 = l1 > l2
           2 = l2 > l1
        */

	TLDE *L1 = duplicar_lista(l1), *L2 = duplicar_lista(l2);
        int comp = comparar_numero_ssinal(L1,L2);
        int tamanho_comp = comp_tam(L1,L2);
        int sinal_comp = comp_sin(L1,L2); 
        TLDE *resp = inicializa(); //lista resposta
       	TNO *sub_point1 = L1->fim, *sub_point2 = L2->fim;
        int empresta = 0, i=0, j=0;

       if (L1->sinal == L2->sinal){
          //sinais iguais    
		if (!comp)
	       {
			resp->sinal = '+';
			insere_inicio(resp, 0);
			return (resp);
	       }
		else if (comp == 1)
		{ // l1>l2 
                    resp -> sinal = L1 -> sinal;
		}
		else{if(L1->sinal == '+'){resp->sinal = '-';}else{resp->sinal = '+';}}
		while (tamanho_comp)
		{ //compara tamanho das listas e insere 0 a esquerda da menor
			if (tamanho_comp>1){insere_inicio(L1,0);}
			else{insere_inicio(L2,0);}
			tamanho_comp = comp_tam(L1,L2);
		}
              while (sub_point1)
	      {
		      if (comp == 1)
		      {		
			i = sub_point1->info-sub_point2->info-empresta;
			empresta = 0;
			if (i < 0)
			{
				i+=10;
				empresta++;
			}
		      }
		      else
		      {	
			i = sub_point2->info-sub_point1->info-empresta;
			empresta = 0;
			if (i < 0)
			{
				i+=10;
				empresta++;
			}
		      }
			sub_point1=sub_point1->ant;
			sub_point2=sub_point2->ant;
			insere_inicio(resp,i);
	      }
	      return (resp);

/*		for (i=0; i<=(apont); i++) {
*		    v = (l1 -> fim - empresta - l2 -> fim);
*		    if (l1 -> fim > 0)
*SOMEBODY	empresta = 0;
*TOUCHA 	    if (v < 0) {
*MY			v = v + 10;
*SPAGHET	 	empresta = 1;
*		    }
*
*		    resp->fim = (char) v % 10;
*        }
*      
*/
       }



       else
       { //sinais diferentes
	      TLDE *Laux = duplicar_lista(l2);
		if(l2->sinal == '+'){Laux->sinal = '-';}else{Laux->sinal = '+';}
		return(soma(L1,Laux));
		
        }
	
}        


int tamanho_lista(TLDE *l){

	int tamanho = 0;

	TNO *p = l-> ini;

	while(p){
		tamanho += 1;
		p = p->prox;
	}

	return tamanho;
}

TLDE *duplicar_lista(TLDE *l){

	TLDE *resp = inicializa();
	TNO *p = l->ini;

	resp -> sinal = l->sinal;

	while(p){
		inserir_fim(resp, p->info);
		p = p->prox;
	}

	return resp;
}

void retirar_elementos(TLDE * l, int x){

	int i;

	for(i=0; i<x; i++){

		TNO *p = l->ini;
		l -> ini = l -> ini -> prox;
		free(p);

	}

}

int trasformar_inteiro(TLDE *l, int x){

	TNO *p = l->ini;

	int numero, i;

	numero = p->info;

	for(i=0; i<x-1; i++){

		numero = numero * 10;
		p = p -> prox;
		numero = numero + p->info;

	}

	return numero;
}

TLDE *divisao2(TLDE *l1, TLDE *l2){

	char positivo = '+', negativo = '-'; // Compara os sinais

	TLDE *duplicata = duplicar_lista(l1), *resp = inicializa(); // Lista de resposta e lista duplicada a lista 1.

	int resto, quociente, dividendo, divisor, calculo, tam_l2 = tamanho_lista(l2),verifica, tam_duplicata, vezes = tam_l2, zero = 0;

	divisor = trasformar_inteiro(l2, tam_l2); // Trasnsforma a lista 2 em um inteiro

	// Calcular sinal da resposta
	resp -> sinal = '+';
	if((l1 -> sinal == negativo) || (l2 -> sinal == negativo)) resp -> sinal = '-';
	if((l1 -> sinal == negativo) && (l2 -> sinal == negativo)) resp -> sinal = '+';

	dividendo = trasformar_inteiro(duplicata, tam_l2);

	tam_duplicata = tamanho_lista(duplicata);

	do{

		if(divisor > dividendo){ // Se o divisor for maior que o dividendo temos que pegar mais um numero da lista

			while(divisor > dividendo){ 

				vezes += 1;

				if(vezes <= tam_duplicata){
	
					dividendo = trasformar_inteiro(duplicata, vezes);
					inserir_fim(resp, 0); // Adicionar 0 na resposta
					
				}
				else{

					libera_lista(duplicata);
					inserir_fim(resp, 0);
					return resp;
				}
			}
		}

		// Realizar o calculo de divisão
		quociente = dividendo / divisor;
		calculo = quociente * divisor;
		resto = dividendo - calculo;

		inserir_fim(resp, quociente);

		retirar_elementos(duplicata, vezes);

		if (resto >= 10){

			while(resto != 0){

				// Sera o resto em digitos e inserir separadamente na lista duplicada da lista 2

				zero += 1;

				int aux = resto % 10;
				insere_inicio(duplicata, aux);
				resto = resto / 10;
			
			}

		}

		else{

			insere_inicio(duplicata, resto);
			zero = 1;

		}

		vezes = zero + 1;
		zero = 0;

		tam_duplicata = tamanho_lista(duplicata);
		// Vezes é a quantide de digitos que deve pergar da lista 1.

		if(vezes > tam_duplicata){ // Se a quantidade de vezes for maior que o tamanho da lista enão a conta passa a ser fracionaria. Porém não vamos utilizar a parte fracionaria

			// Forma de sair do while
			libera_lista(duplicata);
			return resp;
		}

		dividendo = trasformar_inteiro(duplicata, vezes);

		//verifica = comparar_numero_ssinal(duplicata, l2);

	}while(1);

	libera_lista(duplicata);
	
	return resp;
}


TLDE *divisao(TLDE *l1, TLDE *l2){

	/*
	0 = iguais
	1 = l1 > l2
	2 = l2 > l1
	*/

	// Incompleto

	char positivo = '+', negativo = '-';

	int comp = comparar_numero_ssinal(l1, l2), taml2 = tamanho_lista(l2);

	if(taml2 == 1){

		if(l2 -> ini -> info == 0){

			TLDE *resp = inicializa();
			return resp;
		}
	}

	if(comp == 0){
		// Se os dois numeros forem iguais a resposta é +1 ou -1.
		TLDE *resp = inicializa(); // Criar uma nova lista
		insere_inicio(resp, 1); // Inserir o elemento 1 na lista
		resp -> sinal = '+'; // Sinal base
		if((l1 -> sinal == negativo) || (l2 -> sinal == negativo)) resp -> sinal = '-'; // se um dos sinais forem negativos a resposta também é negativa
		if((l1 -> sinal == negativo) && (l2 -> sinal == negativo)) resp -> sinal = '+'; // se os dois sinais forem negativos a resposta é positiva.
		return resp; // Retornar a lista resposta

	}

	if(comp == 2){
		// Se o divisor for maior que o dividendo a resposta vai ser < 0. 0,... Porém utilizamos a parte fracionaria
		TLDE *resp = inicializa();
		primeiro_zero(resp);
		resp -> sinal = '+';
		if((l1 -> sinal == negativo) || (l2 -> sinal == negativo)) resp -> sinal = '-';
		if((l1 -> sinal == negativo) && (l2 -> sinal == negativo)) resp -> sinal = '+';
		return resp;
	}

	if (comp == 1){

		int tam_l1 = tamanho_lista(l1), tam_l2 = tamanho_lista(l2); 

		if(tam_l2 < 9){

			TLDE *resp = divisao2(l1, l2);
			return resp;
		}

		/* O objetivo é ir somando até achar um numero mair ou igual ao divisor */

		char sinal = '=', positivo = '+', negativo = '-'; // para verificar sinal
		TLDE *cont = inicializa(), *somal = inicializa(), *resp = inicializa();

		insere_inicio(cont, 1); // Inserir 1 na lista de cont (vai contar a quantidade de vezes que precisa somar).
		insere_inicio(resp, 1); // Lista de resposta. Pelo fato de o dividendo ser mairo que o divisor a resposta só pode ser 1 ou mairo que 1
		primeiro_zero(somal); // Lista de soma. 

		int compara = 1; // Comparar uma lista com a outra

		// O Sinal das listas que vão ser somadas devem ser positivo.
		if(l2 -> sinal != '+'){
			sinal = l2 -> sinal;
			l2 -> sinal = '+';
		}

		resp -> sinal = '+';
		somal -> sinal = '+';
		cont -> sinal = '+';

		// A lista de soma começa com l2.
		somal = soma(somal, l2);

		// Loop para soma. Só sai do loop se a lista soma for maior ou igual que a lista 1(dividendo)
		do{

			resp = soma(resp, cont);
			somal = soma(somal, l2);
			compara = comparar_numero_ssinal(l1, somal);
		}while(compara == 1);

		// Se compara é igual a 2 é porque a lista soma é maior que o dividendo. Isso quer dizer que somou uma vez a mais. Então deve tirar 1 da resposta
		if (compara == 2) resp = sub(resp, cont);

		if(sinal != '=') l2 -> sinal = sinal;

		if((l1 -> sinal == negativo) || (l2 -> sinal == negativo)) resp -> sinal = '-';
		if((l1 -> sinal == negativo) && (l2 -> sinal == negativo)) resp -> sinal = '+';

		libera_lista(somal);
		libera_lista(cont);

		return resp; 

	}
}


