#include "TRLDE.h"

int menuterminal(){

	int n;

	printf("\t---Menu---\t\n");
	printf("1 - Multiplicação\n2 - Divisão\n3 - Subtração\n4 - Soma\n\nNúmero da opção: ");
	scanf("%d", &n);
	printf("\n");

	return n;

}

int main(){

	int numero;


	TLDE *l1 = inicializa();
	TLDE *l2 = inicializa();

		
	char *n1 = escrever_numero();
	char *n2 = escrever_numero();

	l1 = criar_lista(n1);
	l2 = criar_lista(n2);

	printf("\n");

	free(n1);
	free(n2);

	numero = menuterminal(numero);


	if(numero == 1){

		if((l1->ini) && (l2->ini)){

			system("clear");
			printf("\n");	
	
			TLDE *liasta_multiplica = multiplicar(l1, l2);

			printf("N1: ");
			imprime(l1);
			printf("N2: ");
			imprime(l2);
			printf("Multiplicação: ");
			imprime(liasta_multiplica);
			printf("\n");

			libera_lista(liasta_multiplica);

		}

		else{

			system("clear");

			printf("\nNão foi informado os números para serem multiplicados\n\n");

		}

	}

	if(numero == 2){

		if((l1->ini) && (l2->ini)){

			system("clear");

			printf("\n");	

			TLDE *lista_divisao = divisao(l1, l2);
			printf("N1: ");
			imprime(l1);
			printf("N2: ");
			imprime(l2);
			if(lista_divisao -> ini){

				printf("Divisão: ");
				imprime(lista_divisao);
			}
			else printf("ERRO !! A divisão não pode ser completada.\n");
			printf("\n");
			libera_lista(lista_divisao);

		}

		else{

			system("clear");

			printf("\nNão foi informado os números para serem divididos\n\n");

		}
	}

	if(numero == 3){

		if((l1->ini) && (l2->ini)){

			system("clear");

			printf("\n");	

			TLDE *lista_sub = sub(l1, l2);
			printf("N1: ");
			imprime(l1);
			printf("N2: ");
			imprime(l2);
			printf("Subtração: ");
			imprime(lista_sub);
			printf("\n");

			libera_lista(lista_sub);

		}

		else{

			system("clear");

			printf("\nNão foi informado os números para serem subtraidos\n\n");

		}
	}

	if(numero == 4){

		if((l1->ini) && (l2->ini)){

			system("clear");

			printf("\n");	

			TLDE *lista_soma = soma(l1, l2);
			printf("N1: ");
			imprime(l1);
			printf("N2: ");
			imprime(l2);
			printf("Soma: ");
			imprime(lista_soma);
			printf("\n");

			libera_lista(lista_soma);

		}

		else{

			system("clear");

			printf("\nNão foi informado os números para serem somados\n\n");

		}
	}

	libera_lista(l1);
	libera_lista(l2);

}
