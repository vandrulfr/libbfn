# Lib_BFN
---

# README #

Projeto de Programação Estruturada para calculos com numeros muito grandes

### OBJETIVOS ###

* Input e Output de Números Grandes
* Operações com Números Grandes

---

## GRUPO ##

### Grupo 7 ###

* Artur Melo - Soma, Subtração, Miscelânea
* Barbara Guarino - Multiplicação, Divisão, Miscelânea
* Pedro Henrique Bazilio - Subtração, Miscelânea

---

## DOCUMENTAÇÃO

### MAIN ###

Ao executar o programa, é pedido que o usuário entre dois números, e após isso é exibido um menu.
![Menu](https://bitbucket.org/vandrulfr/libbfn/raw/6abefe78400f71b286ec2e277675c303ef55e894/.tutorialimg/2018-11-25-162337_574x362_scrot.png)

O usuário então seleciona a operação que deseja executar, e são exibidos os números, e a resposta, assim como a operação que foi realizada.
![Resposta](https://bitbucket.org/vandrulfr/libbfn/raw/9cf818800d3c7276b877b7339cc1f5cd12a7420c/.tutorialimg/2018-11-25-162357_574x362_scrot.png)


## ALGORITMO ##

### FUNÇÕES PRINCIPAIS ###

1. Inicializar lista.
2. Inserir elemento no final.
3. Inserir elemento no inicio.
4. Imprimir lista do início -> fim.
5. Escrever número (vetor str).
6. Transformar uma string em uma lista duplamente encadeada
7. Liberar lista.
8. Duplicar lista
9. Calcular o tamanho da lista.
10. Transformar em inteiro um pedaço da lista.
11. Retirar os x primeiros elemento da lista.

---

### SOMA ###

1. Comparar sinais dos numeros. Caso sejam diferentes, subtrai.
2. Se sinais são iguais, define o sinal do resultado como o sinal das listas e executa o próximo passo.
3. Adicionar 0s a esquerda da lista/numero menor para deixa-las com mesmo numero de caracteres.
4. Somar os ultimos elementos de cada lista, e somar à isso digitos excedentes.
5. Se o resultado for superior à 10, guardar digito excedente, subtrair 10 do resultado.
6. Resultado das operações anteriores é enviado para lista de resposta.
7. Ponteiros indicando numero a ser somado se movem para a esquerda.
8. Repetir passos 3-6 até alcançar o inicio da lista.

---

### SUBTRAÇÃO ###

1. Compara sinais dos numeros. Caso sejam diferentes, muda o sinal do segundo numero e os soma.
2. Se sinais são iguais, compara tamanho dos numeros.
    * Se o primeiro for maior, define sinal como positivo e subtrai ele pelo segundo 
    * Se o segundo for maior, define sinal como negativo e subtrai ele pelo primeiro 
3. Adiciona 0s a esquerda do numero de menor comprimento para deixa-los com o mesmo numero de caracteres.
4. Subtrai os ultimos elementos de cada lista, e a isso 1 se o numero anterior tiver "pego emprestado".
    * Se o resultado for menor que 0, é somado 10 e será subtraído 1 da próxima subtração. 
5. Resultado das operações anteriores é enviado para a lista de resposta.
6. Ponteiros indicando numeros a serem subtraídos se movem para a esquerda.
7. Repetir passos 3-6 até alcançar o inicio da lista.

---

### DIVISÃO ###

1. Verificar qual número é maior (divisor -> lista 2 ou dividendo -> lista 1)
2. Se o divisor for maior.
    1. Calcula o sinal.
    2. Retorna a lista de resposta contendo 0.
3. Se o dividendo for igual ao divisor.
    1. Calcula o sinal.
    2. Retorna a lista de resposta contendo 1.
4. Se o dividendo for maior
    1. Calcular o número de dígitos do divisor (lista 2).
    2. Se o numero de dígitos da lista 2 for menor que 9.
        1. Transformar a lista 2 em um número inteiro chamado divisor com x dígitos.
        2. Transformar os x primeiros dígitos da lista 1 em um inteiro chamado dividendo.
        3. Dividir o dividendo pelo divisor, a resposta é colocada no início da lista de resposta.
        4. Retirar os x primeiros elementos da lista 1. E colocar no inicio da lista 1 o resto do calculo da divisão do dividendo pelo divisor.
        5. Continuar enquanto a lista 1 for maior que a lista 2.
    3. Se a lista 2 possuir mais 9 ou mais dígitos.
        1. Somar a lista 2 (divisor) x vezes até que seja igual ou maior que a lista 1.
        2. x será a resposta. 

---

### MULTIPLICAÇÃO###

1. Criar uma lista de resposta e outra de soma.
2. Um loop enquanto não terminar de percorrer a lista l2.
    * Cada elemento da lista l2 de trás para frente é multiplicado pela lista l1 e sua resposta é adicionado na lista soma.
    * Adiciona a quantidade de zeros necessários no final da lista soma. 
    * Soma a lista soma mais a lista resposta.
3. Libera a lista soma.
4. Compara os sinais.
5. Retorna a lista de resposta.

